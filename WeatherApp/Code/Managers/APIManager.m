//
//  APIManager.m
//  WeatherApp
//
//  Created by Serbin Taras on 2/18/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import "APIManager.h"

NSString *const API_URL = @"https://api.openweathermap.org/data/2.5/";

@implementation APIManager

+(void)makeRequestWithMethod:(NSString *)method completionHandler:(void (^)(NSDictionary* data, NSError *error))complete {
    NSString *url = [NSString stringWithFormat:@"%@/%@", API_URL, method];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (httpResponse.statusCode == 200) {
            NSError *parseError = nil;
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
            if (responseDictionary) {
                complete(responseDictionary, nil);
            }
            if (parseError) {
                complete(nil, parseError);
            }
        } else {
            if (error) {
                complete(nil, error);
            }
            complete(nil, [APIManager unknownError]);
        }
    }];
    [dataTask resume];
}

+(NSError *)unknownError {
    NSString *domain = @"com.frigentis.WeatherApp.ErrorDomain";
    NSString *desc = NSLocalizedString(@"Unable to complete the process", @"");
    NSDictionary *userInfo = @{ NSLocalizedDescriptionKey : desc };
    return [NSError errorWithDomain:domain code:-1 userInfo:userInfo];
}

@end
