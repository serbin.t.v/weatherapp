//
//  APIManager.h
//  WeatherApp
//
//  Created by Serbin Taras on 2/18/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface APIManager : NSObject
//make all requests in project
+(void) makeRequestWithMethod:(NSString *)method completionHandler:(void (^)(NSDictionary* data, NSError *error))complete;
@end

NS_ASSUME_NONNULL_END
