//
//  WAWeatherData.m
//  WeatherApp
//
//  Created by Serbin Taras on 2/18/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import "WAWeatherData.h"

//JSON Keys
NSString *const JSONWeatherDescriptionKey = @"weather.description";
NSString *const JSONWeatherTemperatureKey = @"main.temp";
NSString *const JSONWeatherPresureKey = @"main.pressure";
NSString *const JSONWeatherHumadityKey = @"main.humidity";
NSString *const JSONWeatherWindSpeedKey = @"wind.speed";
NSString *const JSONCityName = @"name";
NSString *const JSONWeatherDateKey = @"dt";



@interface WAWeatherData ()
@property (strong, nonatomic) NSString *descr;
@property (strong, nonatomic) NSString *city;
@property (nonatomic, assign) NSInteger presure;
@property (nonatomic, assign) Float32 humidity;
@property (nonatomic, assign) Float32 temperature;
@property (strong, nonatomic) NSDate *date;
@property (nonatomic, assign) Float32 windSpeed;
@end


@implementation WAWeatherData

-(instancetype)initFromDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self) {
        self.city = dictionary[JSONCityName];
        NSTimeInterval dateCreate = [dictionary[JSONWeatherDateKey] doubleValue];
        self.date = [NSDate dateWithTimeIntervalSince1970:dateCreate];
        self.descr = [[dictionary valueForKeyPath:JSONWeatherDescriptionKey] firstObject];
        self.temperature = [[dictionary valueForKeyPath:JSONWeatherTemperatureKey] floatValue];
        self.humidity = [[dictionary valueForKeyPath:JSONWeatherHumadityKey] floatValue];
        self.windSpeed = [[dictionary valueForKeyPath:JSONWeatherWindSpeedKey] floatValue];
        self.presure = [[dictionary valueForKeyPath:JSONWeatherPresureKey] integerValue];
    }
    return self;
}



@end
