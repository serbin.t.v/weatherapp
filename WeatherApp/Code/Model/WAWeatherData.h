//
//  WAWeatherData.h
//  WeatherApp
//
//  Created by Serbin Taras on 2/18/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WAWeatherDataRepresentable.h"

NS_ASSUME_NONNULL_BEGIN

@interface WAWeatherData : NSObject <WAWeatherDataRepresentable>

-(instancetype)init __unavailable;
-(instancetype)initFromDictionary:(NSDictionary *)dictionary NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
