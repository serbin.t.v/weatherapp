//
//  WAWeatherDataService.m
//  WeatherApp
//
//  Created by Serbin Taras on 2/19/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import "WAWeatherDataService.h"
#import "APIManager.h"

NSString *const WEATHER_METHOD_PATH = @"weather?q={CITY_NAME}&units=metric&appid={API_KEY}";
NSString *const API_KEY = @"3f124d9888d433a0276ffc002811697b";

@implementation WAWeatherDataService
+(void)fetchWeatherForCity:(NSString *)city completionHandler:(void (^)(NSDictionary * _Nonnull, NSError * _Nonnull))complete {
    NSString *method = WEATHER_METHOD_PATH;
    method = [method stringByReplacingOccurrencesOfString:@"{CITY_NAME}" withString:city];
    method = [method stringByReplacingOccurrencesOfString:@"{API_KEY}" withString:API_KEY];
    
    [APIManager makeRequestWithMethod:method completionHandler:complete];
    
}
@end
