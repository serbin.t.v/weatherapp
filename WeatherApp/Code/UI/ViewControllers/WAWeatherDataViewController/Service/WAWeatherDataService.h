//
//  WAWeatherDataService.h
//  WeatherApp
//
//  Created by Serbin Taras on 2/19/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WAWeatherDataService : NSObject
+(void)fetchWeatherForCity:(NSString *)city completionHandler:(void (^)(NSDictionary* data, NSError *error))complete;
@end

NS_ASSUME_NONNULL_END
