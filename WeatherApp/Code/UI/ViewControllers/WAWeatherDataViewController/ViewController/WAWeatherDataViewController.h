//
//  WAWeatherDataViewController.h
//  WeatherApp
//
//  Created by Serbin Taras on 2/19/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WAWeatherDataView.h"

NS_ASSUME_NONNULL_BEGIN

@interface WAWeatherDataViewController : UIViewController <WAWeatherDataView>

@end

NS_ASSUME_NONNULL_END
