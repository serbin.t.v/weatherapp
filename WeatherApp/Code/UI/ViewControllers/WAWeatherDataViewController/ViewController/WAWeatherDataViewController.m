//
//  WAWeatherDataViewController.m
//  WeatherApp
//
//  Created by Serbin Taras on 2/19/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import "WAWeatherDataViewController.h"
#import "WAWeatherPresenter.h"
#import "WAWeatherData.h"

NSInteger const kActivityIndicatorTag = 1001;
NSString *const kCityName = @"Moscow";
NSString *const kDatePrefix = @"на:";

@interface WAWeatherDataViewController ()
#pragma Presenter 
@property (nonatomic, strong) WAWeatherPresenter *presenter;

#pragma Oultets
@property (weak, nonatomic) IBOutlet UILabel *lblWeatherDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTemp;
@property (weak, nonatomic) IBOutlet UILabel *lblPresure;
@property (weak, nonatomic) IBOutlet UILabel *lblHumadity;
@property (weak, nonatomic) IBOutlet UILabel *lblWindSpeed;
@property (weak, nonatomic) IBOutlet UILabel *lblDescr;

@end

@implementation WAWeatherDataViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    _presenter = [[WAWeatherPresenter alloc] initWithView:self];
    [self updateWeather];
}

//load data
-(void)updateWeather {
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view setUserInteractionEnabled:NO];
    [activityIndicator setHidesWhenStopped:YES];
    activityIndicator.center = self.view.center;
    activityIndicator.tag = kActivityIndicatorTag;
    [_presenter updateWeatherForCity:kCityName];
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

//show alert when load failed
-(void)showAlertWithError:(NSString *)error {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:error preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:action];
    [self presentViewController:alert animated:true completion:nil];
}

//Remove activity indicator when load copleted
-(void)removeActivityIndicator {
    UIActivityIndicatorView *activityIndicator = [self.view viewWithTag:kActivityIndicatorTag];
    [activityIndicator removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
}

//mark update weather dataUI
-(void)updateUIWithWeatherData:(id<WAWeatherDataRepresentable>)data {
    NSDateFormatter *const dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd.MM.YYYY HH:mm";
    NSDate *weatherDate = [data date];
    _lblWeatherDate.text = [NSString stringWithFormat:@"%@ %@", kDatePrefix, [dateFormatter stringFromDate:weatherDate]];
    _lblTemp.text = [NSString stringWithFormat:@"%.f ℃", data.temperature];
    _lblDescr.text = data.descr;
    _lblPresure.text = [NSString stringWithFormat:@"%li hPa", data.presure];
    _lblHumadity.text = [NSString stringWithFormat:@"%.f %%", data.humidity];
    _lblWindSpeed.text = [NSString stringWithFormat:@"%.f m\\s", data.windSpeed];
}

//refresh weather data
- (IBAction)btnRefreshTapped:(UIButton *)sender {
    [self updateWeather];
}

#pragma MARK: - WAWeatherDataViewProtocol

- (void)didFailedLoadWeatherDataWithError:(nonnull NSError *)error {
    [self removeActivityIndicator];
    [self showAlertWithError:error.localizedDescription];
}

- (void)didLoadWeatherData:(id<WAWeatherDataRepresentable>)data {
    [self removeActivityIndicator];
    [self updateUIWithWeatherData:data];
}

@end
