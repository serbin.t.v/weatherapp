//
//  WAWeatherDataView.h
//  WeatherApp
//
//  Created by Serbin Taras on 2/19/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol WAWeatherDataRepresentable;
NS_ASSUME_NONNULL_BEGIN

@protocol WAWeatherDataView <NSObject>

@required
//data loaded
-(void)didLoadWeatherData:(id<WAWeatherDataRepresentable>)data;
//filed load data
-(void)didFailedLoadWeatherDataWithError:(NSError *)error;

@end

NS_ASSUME_NONNULL_END
