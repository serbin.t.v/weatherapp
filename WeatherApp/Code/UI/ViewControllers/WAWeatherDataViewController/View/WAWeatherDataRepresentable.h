//
//  WAWeatherDataRepresantable.h
//  WeatherApp
//
//  Created by Serbin Taras on 2/19/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol WAWeatherDataRepresentable <NSObject>
@property (strong, nonatomic, readonly) NSString *descr;
@property (strong, nonatomic, readonly) NSString *city;
@property (nonatomic, assign, readonly) NSInteger presure;
@property (nonatomic, assign, readonly) Float32 humidity;
@property (nonatomic, assign, readonly) Float32 temperature;
@property (strong, nonatomic, readonly) NSDate *date;
@property (nonatomic, assign, readonly) Float32 windSpeed;
@end

NS_ASSUME_NONNULL_END
