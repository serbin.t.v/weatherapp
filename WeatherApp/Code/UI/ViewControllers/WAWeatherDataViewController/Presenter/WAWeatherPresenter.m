//
//  WAWeatherPresenter.m
//  WeatherApp
//
//  Created by Serbin Taras on 2/19/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import "WAWeatherPresenter.h"
#import "WAWeatherDataService.h"
#import "WAWeatherDataView.h"
#import "WAWeatherData.h"

@implementation WAWeatherPresenter

-(WAWeatherPresenter *)initWithView:(id<WAWeatherDataView>)view {
    self = [super init];
    if (self) {
        self.weatherView = view;
        
    }
    return self;
}

-(void)updateWeatherForCity:(NSString *)city {
    __weak typeof(self) weakSelf = self;
    [WAWeatherDataService fetchWeatherForCity:city completionHandler:^(NSDictionary * data, NSError * error) {
        __strong typeof(self) strongSelf = weakSelf;
        dispatch_async(dispatch_get_main_queue(), ^{
            if(strongSelf)
            {
                if(error)
                {
                    if ([strongSelf.weatherView respondsToSelector:@selector(didFailedLoadWeatherDataWithError:)]) {
                        
                        //return error to view controller
                        [strongSelf.weatherView didFailedLoadWeatherDataWithError:error];
                    }
                } else {
                    if ([strongSelf.weatherView respondsToSelector:@selector(didLoadWeatherData:)]) {
                        WAWeatherData *weatherData = [[WAWeatherData alloc] initFromDictionary:data];
                        
                        //return correct data to viewcontroller
                        [strongSelf.weatherView didLoadWeatherData:weatherData];
                    }
                }
            }
        });
    }];
}
@end
