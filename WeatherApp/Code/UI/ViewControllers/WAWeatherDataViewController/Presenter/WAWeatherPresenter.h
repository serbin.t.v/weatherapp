//
//  WAWeatherPresenter.h
//  WeatherApp
//
//  Created by Serbin Taras on 2/19/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol WAWeatherDataView;

NS_ASSUME_NONNULL_BEGIN

@interface WAWeatherPresenter : NSObject

@property(nonatomic, weak) id<WAWeatherDataView> weatherView;
- (instancetype)init __unavailable;
-(WAWeatherPresenter *)initWithView:(id <WAWeatherDataView>)view NS_DESIGNATED_INITIALIZER;

//request weather for city
-(void)updateWeatherForCity:(NSString *)city;

@end

NS_ASSUME_NONNULL_END
