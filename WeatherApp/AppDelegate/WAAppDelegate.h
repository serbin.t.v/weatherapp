//
//  AppDelegate.h
//  WeatherApp
//
//  Created by Serbin Taras on 2/18/19.
//  Copyright © 2019 Serbin Taras. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

